<?php

$_DEFAULT_KEY = "youmustresist";
$_DEFAULT_CIPHERTEXT = implode("\n", array(
    "OSOEOETDCARCI",
    "LNSTFFDOAFKTH",
    "OLGABIMBALWIA",
    "GEHROEAESDLIL",
    "LICEIMRDTSEAE",
    "SDTARNEERHALG",
    "EOOTRAGRAORGT",
    "ANFNHENDADNEE",
    "SNTTEIODOTSEE",
    "KTBLGEVEYLLEN",
    "EIUSHOSFWWEDF",
    "INGLAENFEENMO",
    "NVDNLNHMAAWCE",
));

$key = isset($_POST["txtKey"]) ? $_POST["txtKey"] : $_DEFAULT_KEY;
$cipherText = isset($_POST["txtCipher"]) ? $_POST["txtCipher"] : $_DEFAULT_CIPHERTEXT;

?>

<!DOCTYPE html>
<html>
<head>
    <title>HCF013</title>
    <style>
        h4 {
            margin-bottom: 8px;
        }
        
        #topWrapper {
            display: flex;
            flex-flow: row;
            align-items: stretch;
        }
        
        #methods {
            margin-left: 20px;
            flex-grow: 1;
        }
    </style>
</head>
<body>
    <div id="topWrapper">
        <form id="frmCipher" method="post">
            <label for="txtKey">Key:</label><br>
            <input type="text" name="txtKey" id="txtKey" value="<?= $key ?>"/><br>
            
            <label for="txtCipher">Cipher text:</label><br>
            <textarea name="txtCipher" id="txtCipher" rows="14" style="width: 300px"><?= $cipherText ?></textarea><br>

            <button id="btnDecipher">Decipher</button>
        </form>
        
        <div id="methods">
            <h4>Method 1</h4>
            <p>
                Creates a matrix with a width equal to the key length. Populates the matrix
                with the cipher-text row-by-row, left to right and top to bottom.<br>
                
                Then it reads vertically from top to bottom, using the alphabetical order of
                the letters in the key as the column order.
            </p>
            
            <h4>Method 2</h4>
            <p>
                Creates a matrix with a width equal to the key length. Populates the matrix
                column-by-column, top-to-bottom, using the alphabetical order of the letters
                in the key as the column order.<br>
                
                Then it reads row-by-row, left to right and top to bottom.
            </p>
            
            <h4>Method 3</h4>
            <p>
                Creates a matrix with a width equal to the key length. Populates the matrix
                with the cipher-text column-by-column, top to bottom and left to right.<br>
                
                Reads row-by-row, using the alphabetical order of the letters in the key as
                the column order for each row.
            </p>
            
            <h4>Method 4</h4>
            <p>
                Creates a matrix with a width equal to the key length. Populates the matrix
                with the cipher-text row-by-row, left to right and top to bottom.<br>
                
                Reads row-by-row, using the alphabetical order of the letters in the key as
                the column order for each row.
            </p>
        </div>
    </div>
    <br><br>
    <form id="frmResults">
        <label for="txtResults">Results:</label><br>
        <textarea name="txtResults" id="txtResults" rows="30" style="width: 80vw"></textarea>
    </form>

    <?php include "common/libs.php"; ?>
    <script src="/dist/js/index.js?v=<?= $libsVersion ?>"></script>
</body>
</html>