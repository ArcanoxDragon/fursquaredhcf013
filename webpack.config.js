require( "ts-node" ).register( {
    compilerOptions: {
        target: "es5",
        module: "commonjs",
        esModuleInterop: true,
        lib: [
            "es5",
            "es6",
            "es2015",
            "esnext",
        ],
    },
} );

module.exports = require( "./webpack.config.ts" );