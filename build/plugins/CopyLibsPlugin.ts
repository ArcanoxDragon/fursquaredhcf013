import webpack from "webpack";
import CopyPlugin from "copy-webpack-plugin";

export interface LibDependency {
    name: string;
    globs: string[];
}

export interface Options {
    libs: LibDependency[];
    outputPath?: string;
}

const DefaultOptions: Partial<Options> = {
    outputPath: "lib",
};

export default class CopyLibsPlugin implements webpack.Plugin {
    private options: Options;

    constructor( options: Options ) {
        this.options = Object.assign( DefaultOptions, options );
    }

    apply( compiler: webpack.Compiler ): void {
        let copyItems = this.options.libs.flatMap( lib => {
            let libBasePath = `node_modules/${lib.name}`;
            let targetPath = `lib/${lib.name}`;

            return lib.globs.map( glob => ( {
                context: libBasePath,
                from: glob,
                to: targetPath,
                toType: "dir" as "dir", // Stupid hackery required for type compliance since CopyPlugin doens't expose its types
                flatten: true,
            } ) );
        } );

        // Let CopyPlugin do the actual work
        new CopyPlugin( copyItems ).apply( compiler );
    }
}