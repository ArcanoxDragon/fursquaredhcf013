import globby from "globby";
import path from "path";

export default function findScriptEntryPoints(): object {
    const srcDir = path.join( __dirname, "../../src" );
    const entryDir = path.join( srcDir, "main" );
    const files = globby.sync( [ "**/*.{ts,tsx}" ], { cwd: entryDir } );
    const entries: any = {};

    files.forEach( file => {
        let nameMatch = file.match( /(.*)\.[jt]sx?$/i );

        entries[ `js/${nameMatch[ 1 ]}` ] = `./src/main/${file}`
    } );

    return entries;
}