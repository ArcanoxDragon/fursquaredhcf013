import globby from "globby";
import path from "path";

export default function findStyleEntryPoints(): object {
    const stylesDir = path.join( __dirname, "../../styles" );
    const files = globby.sync( [ "**/*.scss", "!**/_*.scss" ], { cwd: stylesDir } );
    const entries: any = {};

    files.forEach( file => {
        let nameMatch = file.match( /(.*)\.scss?$/i );

        entries[ `css/${nameMatch[ 1 ]}` ] = `./styles/${file}`
    } );

    return entries;
}