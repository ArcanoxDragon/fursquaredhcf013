<?php
    $libsVersion = 2;
?>

<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="/dist/js/manifest.js?v=<?= $libsVersion ?>"></script>
<script src="/dist/js/vendor.js?v=<?= $libsVersion ?>"></script>
<script src="/dist/js/react-mount.js?v=<?= $libsVersion ?>"></script>