<!DOCTYPE html>
<html>
<head>
    <title>HCF013</title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
        }
        
        html {
            background-color: black;
            color: #00ff00;
            font-family: "Consolas", "Courier New", monospace;
        }
        
        .wrapper {
            padding: 80px;
        }
        
        button {
            padding: 6px 8px;
            color: #00ff00;
            background-color: #001100;
            border: 1px solid #004400;
            font-family: "Consolas", "Courier New", monospace;
        }
        
        button:not(:disabled) {
            cursor: pointer;
        }
        
        button:disabled {
            color: #008800;
            background: transparent;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div data-app-name="PlaygroundApp"></div>
    </div>

    <?php include "common/libs.php"; ?>
    <script src="/dist/js/playground.js?v=<?= $libsVersion ?>"></script>
</body>
</html>