<?php

$_DEFAULT_CIPHERTEXT = implode("\n", array(
    "OSOEOETDCARCI",
    "LNSTFFDOAFKTH",
    "OLGABIMBALWIA",
    "GEHROEAESDLIL",
    "LICEIMRDTSEAE",
    "SDTARNEERHALG",
    "EOOTRAGRAORGT",
    "ANFNHENDADNEE",
    "SNTTEIODOTSEE",
    "KTBLGEVEYLLEN",
    "EIUSHOSFWWEDF",
    "INGLAENFEENMO",
    "NVDNLNHMAAWCE",
));

$cipherText = isset($_POST["txtCipher"]) ? $_POST["txtCipher"] : $_DEFAULT_CIPHERTEXT;

?>
<!DOCTYPE html>
<html>
<head>
    <title>HCF013</title>
    <style>
        h4 {
            margin-bottom: 8px;
        }
        
        #topWrapper {
            display: flex;
            flex-flow: row;
            align-items: stretch;
        }
        
        #methods {
            margin-left: 20px;
            flex-grow: 1;
        }
    </style>
</head>
<body>
    <form id="frmCipher" method="post">
        <label for="txtCipher">Cipher text:</label><br>
        <textarea name="txtCipher" id="txtCipher" rows="14" style="width: 300px"><?= $cipherText ?></textarea><br>

        <button id="btnDecipher">Shuffle Columns</button>
    </form>
    <br><br>
    <form id="frmResults">
        <label for="txtResults">Results:</label><br>
        <textarea name="txtResults" id="txtResults" rows="30" style="width: 80vw"></textarea>
    </form>

    <?php include "common/libs.php"; ?>
    <script src="/dist/js/columns.js?v=<?= $libsVersion ?>"></script>
</body>
</html>