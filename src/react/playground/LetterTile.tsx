import React from "react";
import classnames from "classnames";

import { makeStyles } from "@material-ui/styles";
import { Letter } from "@src/react/playground/models";

const useStyles = makeStyles( {
    root: {
        backgroundColor: "#001100",
        border: "1px solid #004400",
        position: "relative",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        margin: "0 4px",
        width: 32,
        height: 40,
        cursor: "pointer",
        touchAction: "manipulation",
        userSelect: "none",
    },
    letter: {
        fontSize: 24,
        fontWeight: "bold",
    },
    order: {
        position: "absolute",
        bottom: 2,
        right: 2,
        fontSize: 10,
    },
}, { name: "LetterTile" } );

interface Props {
    className?: string;
    model: Letter;

    onClick?( e: React.MouseEvent ): void;
}

export function LetterTile( { className, model, onClick }: Props ) {
    let classes = useStyles();

    return <div className={ classnames( classes.root, className ) } onClick={ onClick }>
        <div className={ classes.letter }>
            { model.letter }
        </div>
        { typeof model.order === "number" && <div className={ classes.order }>
            { model.order }
        </div> }
    </div>;
}