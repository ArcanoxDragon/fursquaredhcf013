export interface CodeRow {
    key: string;
    code: string;
}

export interface RowModel {
    key: string;
    letters: Letter[];
    played: Letter[];
}

export interface Letter {
    letter: string;
    order: number;
}