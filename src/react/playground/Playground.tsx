import React, { useEffect } from "react";

import { Row } from "@src/react/playground/Row";
import { makeStyles } from "@material-ui/styles";
import { useSelector, useDispatch, useStore, Provider } from "react-redux";
import store, { AppDispatch } from "@src/react/playground/redux/store";
import { Actions } from "@src/react/playground/redux/actions";
import { AppState, DefaultState } from "@src/react/playground/redux/state";
import { Letter, RowModel } from "@src/react/playground/models";

const useStyles = makeStyles( {
    root: {},
    button: {
        marginBottom: 8,
        marginRight: 12,
        fontSize: 16,
    },
    table: {
        width: "100%",
        borderCollapse: "collapse",
        tableLayout: "fixed",

        "& td, & th": {
            padding: 6,
            border: "1px solid #004400",
            textAlign: "center",
        },
    },
}, { name: "Playground" } );

function serializeState( state: AppState ): ( string | number )[][] {
    let { rows } = state;

    return rows.map<( string | number )[]>( row => {
        let played = row.played.map( l => l.order - 1 );
        let data = [ row.key, ...played ];

        return data;
    } );
}

function deserializeState( jsonRows: ( string | number )[][] ): AppState {
    let rows = jsonRows.map<RowModel>( data => {
        let [ key, ...playedIndices ] = data;
        let defaultRow = DefaultState.rows.find( r => r.key === key );;
        let { letters } = defaultRow;
        let played = playedIndices.map( i => letters[ i ] );

        return { key: key.toString(), letters, played };
    } );

    return { rows };
}

function PlaygroundComponent() {
    let classes = useStyles();
    let store = useStore<AppState>();
    let rows = useSelector( ( state: AppState ) => state.rows );
    let dispatch = useDispatch<AppDispatch>();

    function loadStateFromHash() {
        let stateBase64 = window.location.hash.replace( /^#/, "" );

        if ( stateBase64.length === 0 ) {
            return;
        }

        try {
            let stateJson = atob( stateBase64 );
            let state: AppState = deserializeState( JSON.parse( stateJson ) );

            dispatch( Actions.replaceState( state ) );
        } catch ( e ) { console.error( e ) }
    }

    useEffect( () => {
        // One-shot code


        // Subscribe to state changes and update the window hash to reflect the state
        store.subscribe( () => {
            let state = store.getState();
            let stateJson = JSON.stringify( serializeState( state ) );
            let stateBase64 = btoa( stateJson );

            location.hash = stateBase64;
        } );

        // Subscribe to hash changes and update the state
        window.addEventListener( "hashchange", loadStateFromHash );

        // Try and load the state from the hash
        loadStateFromHash();
    }, [] );

    return <div className={ classes.root }>
        <button className={ classes.button } onClick={ () => dispatch( Actions.replaceState( DefaultState ) ) }>
            Reset All
        </button>

        <button className={ classes.button } onClick={ () => dispatch( Actions.resetLetters() ) }>
            Reset Letters
        </button>

        <table className={ classes.table }>
            <thead>
                <tr>
                    <th style={ { width: 80 } }>Move</th>
                    <th>Key Word</th>
                    <th style={ { width: "40%" } }>Letters</th>
                    <th style={ { width: "40%" } }>Playboard</th>
                </tr>
            </thead>
            <tbody>
                { rows.map( ( row, i ) => <Row
                    key={ row.key }
                    model={ row }
                    canMoveUp={ i > 0 }
                    canMoveDown={ i < rows.length - 1 }
                    onClickMoveUp={ () => dispatch( Actions.moveRowUp( row ) ) }
                    onClickMoveDown={ () => dispatch( Actions.moveRowDown( row ) ) } /> ) }
            </tbody>
        </table>
    </div>;
}

export function Playground() {
    return <Provider store={ store }>
        <PlaygroundComponent />
    </Provider>;
}