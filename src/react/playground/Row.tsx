import React from "react";

import { RowModel } from "@src/react/playground/models";
import { makeStyles } from "@material-ui/styles";
import { LetterTile } from "@src/react/playground/LetterTile";
import { useDispatch } from "react-redux";
import { AppDispatch } from "@src/react/playground/redux/store";
import { Actions } from "@src/react/playground/redux/actions";

const useStyles = makeStyles( {
    root: {},
    button: {
        margin: 4,
        fontSize: 16,
        touchAction: "manipulation",
    },
    hint: {
        fontSize: 10,
    },
    letters: {
        display: "flex",
        flexFlow: "row",
        alignItems: "center",
        justifyContent: "flex-start"
    },
    faded: {
        opacity: 0.5,
    },
}, { name: "Row" } );

interface Props {
    model: RowModel;
    canMoveUp: boolean;
    canMoveDown: boolean;
    onClickMoveUp(): void;
    onClickMoveDown(): void;
}

export function Row( { model, canMoveUp, canMoveDown, onClickMoveUp, onClickMoveDown }: Props ) {
    let classes = useStyles();
    let dispatch = useDispatch<AppDispatch>();

    return <tr className={ classes.root }>
        <td>
            <button className={ classes.button } disabled={ !canMoveUp } onClick={ onClickMoveUp }>↑</button>
            <button className={ classes.button } disabled={ !canMoveDown } onClick={ onClickMoveDown }>↓</button>
        </td>
        <td style={ { textAlign: "right" } }>
            { model.key }
            <span className={ classes.hint }>&nbsp;= { model.key.length }</span>
        </td>
        <td>
            <div className={ classes.letters }>
                { model.letters.map( letter => <LetterTile
                    key={ letter.order }
                    className={ model.played.includes( letter ) ? classes.faded : null }
                    model={ letter }
                    onClick={ () => dispatch( Actions.toggleLetterPlayed( model, letter ) ) } /> ) }
            </div>
        </td>
        <td>
            <div className={ classes.letters }>
                { model.played.map( letter => <LetterTile
                    key={ letter.order }
                    model={ letter }
                    onClick={ () => dispatch( Actions.toggleLetterPlayed( model, letter ) ) } /> ) }
            </div>
        </td>
    </tr>;
}