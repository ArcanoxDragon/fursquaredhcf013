import { RowModel, Letter } from "@src/react/playground/models";
import { AppState } from "@src/react/playground/redux/state";

export const REPLACE_STATE = "REPLACE_STATE";
export const MOVE_ROW = "MOVE_ROW";
export const PLAY_LETTER = "PLAY_LETTER";
export const RESET_LETTERS = "RESET_LETTERS";

export interface ReplaceStateAction {
    type: typeof REPLACE_STATE;
    state: AppState;
}

export interface MoveRowAction {
    type: typeof MOVE_ROW;
    row: RowModel;
    delta: number;
}

export interface PlayLetterAction {
    type: typeof PLAY_LETTER;
    row: RowModel;
    letter: Letter;
}

export interface ResetLettersAction {
    type: typeof RESET_LETTERS;
}

export type ActionTypes =
    | ReplaceStateAction
    | MoveRowAction
    | PlayLetterAction
    | ResetLettersAction;

export const Actions = {
    replaceState( state: AppState ): ActionTypes {
        return { type: REPLACE_STATE, state };
    },

    moveRowUp( row: RowModel ): ActionTypes {
        return { type: MOVE_ROW, row, delta: -1 };
    },

    moveRowDown( row: RowModel ): ActionTypes {
        return { type: MOVE_ROW, row, delta: 1 };
    },

    toggleLetterPlayed( row: RowModel, letter: Letter ): ActionTypes {
        return { type: PLAY_LETTER, row, letter };
    },

    resetLetters(): ActionTypes {
        return { type: RESET_LETTERS };
    },
};