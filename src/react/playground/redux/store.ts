import combinedReducer from "@src/react/playground/redux/reducers";

import { createStore } from "redux";

const store = createStore( combinedReducer );

export default store;
export type AppDispatch = typeof store.dispatch;