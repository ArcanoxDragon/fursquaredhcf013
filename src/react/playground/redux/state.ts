import { CodeRow, RowModel, Letter } from "@src/react/playground/models";

export interface AppState {
    rows: RowModel[];
}

const Codes: CodeRow[] = [
    { key: "CON", code: "OSOEOETDCARCI" },
    { key: "GLOMERATE", code: "LNSTFFDOAFKTH" },
    { key: "DESCENDS", code: "OLGABIMBALWIA" },
    { key: "RIGHT", code: "GEHROEAESDLIL" },
    { key: "INTO", code: "LICEIMRDTSEAE" },
    { key: "HELL", code: "SDTARNEERHALG" },
    { key: "DON'T", code: "EOOTRAGRAORGT" },
    { key: "FOLLOW", code: "ANFNHENDADNEE" },
    { key: "THEM", code: "SNTTEIODOTSEE" },
    { key: "DOWN", code: "KTBLGEVEYLLEN" },
    { key: "TO", code: "EIUSHOSFWWEDF" },
    { key: "YOUR", code: "INGLAENFEENMO" },
    { key: "DOOM", code: "NVDNLNHMAAWCE" },
];

export const DefaultState: AppState = {
    rows: Codes.map<RowModel>( row => ( {
        key: row.key,
        letters: row.code.split( "" ).map<Letter>( ( letter, i ) => ( { letter, order: i + 1 } ) ),
        played: [],
    } ) ),
};