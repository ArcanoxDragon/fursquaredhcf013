import { combineReducers, CombinedState } from "redux";
import { ActionTypes, MOVE_ROW, PLAY_LETTER, REPLACE_STATE, RESET_LETTERS } from "@src/react/playground/redux/actions";
import { RowModel } from "@src/react/playground/models";
import { DefaultState, AppState } from "@src/react/playground/redux/state";

function rowsReducer( rows: RowModel[] = DefaultState.rows, action: ActionTypes ): RowModel[] {
    switch ( action.type ) {
        case MOVE_ROW: {
            let { row, delta } = action;
            let index = rows.indexOf( row );
            let newRows = [ ...rows ];

            newRows.splice( index, 1 );
            index += delta;

            if ( index < 0 ) index = 0;
            if ( index >= rows.length ) index = rows.length;

            newRows.splice( index, 0, row );

            return newRows;
        }
        case PLAY_LETTER: {
            let { row, letter } = action;
            let { played } = row;
            let index = rows.indexOf( row );
            let newRows = [ ...rows ];

            if ( played.includes( letter ) ) {
                played = played.filter( l => l !== letter );
            } else {
                played = [ ...played, letter ];
            }

            row = { ...row, played };
            newRows.splice( index, 1, row );

            return newRows;
        }
        case RESET_LETTERS: {
            let newRows = rows.map( row => {
                let { key, letters } = row;

                return { key, letters, played: [] };
            } );
            
            return newRows;
        }
    }

    return rows;
}

const combinedReducer = combineReducers<AppState, ActionTypes>( { rows: rowsReducer } );

export default function reducer( state: AppState, action: ActionTypes ): AppState {
    switch ( action.type ) {
        case REPLACE_STATE: {
            let { state } = action;

            return state;
        }
        default: {
            return combinedReducer( state, action );
        }
    }
}