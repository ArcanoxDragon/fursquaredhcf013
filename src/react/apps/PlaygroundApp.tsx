import React from "react";

import { Playground } from "@src/react/playground/Playground";

export default class PlaygroundApp extends React.Component {
    render() {
        return <Playground />;
    }
}