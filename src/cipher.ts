import "./extensions";

import { findOrder } from "./order";

/**
 * Fills matrix with ciphertext horizontally, then reads off vertically in column order
 * specified by key. 
 */
export function decodeColumnar( ciphertext: string, key: string, iterations: number = 1 ) {
    key = key.replace( /[^a-z]/gi, "" ).toLowerCase();

    let keyOrder = findOrder( key );
    let matrix: string[] = [];

    for ( let i = 0; i < ciphertext.length; i++ ) {
        let char = ciphertext.charAt( i );
        let row = Math.floor( i / key.length );

        if ( row >= matrix.length ) {
            matrix.push( char );
        } else {
            matrix[ row ] += char;
        }
    }

    // Pad matrix
    matrix = matrix.map( m => m.padEnd( key.length, "*" ) );

    // Solve
    let plaintext = "";

    for ( let col of keyOrder ) {
        // Append each row to the plaintext string

        for ( let row of matrix ) {
            plaintext += row.charAt( col );
        }
    }

    let result = plaintext.replace( /\*/g, "" );

    if ( iterations > 1 )
        return decodeColumnar( result, key, iterations - 1 );

    return result;
}

/**
 * Fills matrix with ciphertext vertically in column order specified by key, then
 * reads off horizontally row-by-row.
 */
export function decodeColumnar2( ciphertext: string, key: string, iterations: number = 1 ) {
    key = key.replace( /[^a-z]/gi, "" ).toLowerCase();

    let keyOrder = findOrder( key );
    let numRows = Math.ceil( ciphertext.length / key.length );
    let matrix: string[] = new Array( numRows ).fill( "*".repeat( key.length ) );
    let padChars = Math.floor( ( numRows - ( ciphertext.length / key.length ) ) * key.length );
    let numCharsBottomRow = key.length - padChars;
    let colIndex = 0;
    let row = 0;

    for ( let i = 0; i < ciphertext.length; i++ ) {
        let char = ciphertext.charAt( i );
        let col = keyOrder[ colIndex ];
        let colHeight = col > numCharsBottomRow ? numRows - 1 : numRows;

        matrix[ row ] = matrix[ row ].replaceAt( col, char );

        row++;

        if ( row >= colHeight ) {
            row = 0;
            colIndex++;
        }
    }

    let result = matrix.join( "" ).replace( /\*/g, "" );

    if ( iterations > 1 )
        return decodeColumnar2( result, key, iterations - 1 );

    return result;
}

/**
 * Fills ciphertext in columns left-to-right, then reads off row-by-row in the column order
 * specified by key
 */
export function decodeColumnar3( ciphertext: string, key: string, iterations: number = 1 ) {
    let keyOrder = findOrder( key );
    let numRows = Math.ceil( ciphertext.length / key.length );
    let matrix: string[] = new Array( numRows ).fill( "*".repeat( key.length ) );
    let padChars = Math.floor( ( numRows - ( ciphertext.length / key.length ) ) * key.length );
    let numCharsBottomRow = key.length - padChars;
    let col = 0;
    let row = 0;

    for ( let i = 0; i < ciphertext.length; i++ ) {
        let char = ciphertext.charAt( i );
        let colHeight = col > numCharsBottomRow ? numRows - 1 : numRows;

        matrix[ row ] = matrix[ row ].replaceAt( col, char );

        row++;

        if ( row >= colHeight ) {
            row = 0;
            col++;
        }
    }

    let result = "";

    for ( let row of matrix ) {
        for ( let i = 0; i < key.length; i++ ) {
            let col = keyOrder[ i ];

            result += row.charAt( col );
        }
    }

    result = result.replace( /\*/g, "" );

    if ( iterations > 1 )
        return decodeColumnar3( result, key, iterations - 1 );

    return result;
}

/**
 * Fills matrix with ciphertext horizontally, then reads off row-by-row in the column order
 * specified by key
 */
export function decodeColumnar4( ciphertext: string, key: string, iterations: number = 1 ) {
    key = key.replace( /[^a-z]/gi, "" ).toLowerCase();

    let keyOrder = findOrder( key );
    let matrix: string[] = [];

    for ( let i = 0; i < ciphertext.length; i++ ) {
        let char = ciphertext.charAt( i );
        let row = Math.floor( i / key.length );

        if ( row >= matrix.length ) {
            matrix.push( char );
        } else {
            matrix[ row ] += char;
        }
    }

    // Pad matrix
    matrix = matrix.map( m => m.padEnd( key.length, "*" ) );

    // Solve
    let plaintext = "";

    for ( let row of matrix ) {
        // Append each row to the plaintext string in the right order
        for ( let col of keyOrder ) {

            plaintext += row.charAt( col );
        }
    }

    let result = plaintext.replace( /\*/g, "" );

    if ( iterations > 1 )
        return decodeColumnar4( result, key, iterations - 1 );

    return result;
}

if ( require.main === module ) {
    if ( process.argv.length < 4 ) {
        throw new Error( "Missing input" );
    }

    let [ , , key, ...inputArr ] = process.argv;
    let input = inputArr.join( " " );

    console.log( `Method 1: ${decodeColumnar( input, key )}` );
    console.log( `Method 2: ${decodeColumnar2( input, key )}` );
    console.log( `Method 3: ${decodeColumnar3( input, key )}` );
    console.log( `Method 4: ${decodeColumnar4( input, key )}` );
}