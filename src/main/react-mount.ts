import { ComponentType } from "react";
import { ReactHelpers } from "@arcanox/asp-net-core-helpers";

async function resolveApp( appName: string ): Promise<ComponentType> {
    let appModule = await import( /* webpackChunkName: "js/react/[request]" */ `../react/apps/${appName}` );
    
    return appModule.default;
}

$( () => {
    ReactHelpers.findAndMountApps( resolveApp );
} );