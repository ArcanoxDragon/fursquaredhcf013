import "../extensions";

$( () => {
    let $txtCipher = $( "#txtCipher" );
    let $txtResults = $( "#txtResults" );
    let cipherLines = $txtCipher.val().toString().split( /[\r\n]/ );

    function error( msg: string ) {
        $txtResults.val( `Error: ${msg}` );
    }

    if ( cipherLines.length === 0 ) {
        return error( "No input" );
    }

    if ( !cipherLines.every( line => line.length === cipherLines[ 0 ].length ) ) {
        return error( "All lines must be the same length" );
    }

    // Transpose lines into columns
    let lineLength = cipherLines[ 0 ].length;
    let columns: string[] = new Array( lineLength ).fill( "" );

    for ( let line of cipherLines ) {
        for ( let i = 0; i < line.length; i++ ) {
            columns[ i ] += line.charAt( i );
        }
    }

    let newColumns = columns.randomize();
    let newLines: string[] = new Array( cipherLines.length ).fill( "" );

    for ( let column of newColumns ) {
        for ( let i = 0; i < column.length; i++ ) {
            newLines[ i ] += column.charAt( i );
        }
    }

    $txtResults.val( newLines.join( "\n" ) );
} );