import {
    decodeColumnar,
    decodeColumnar2,
    decodeColumnar3,
    decodeColumnar4,
} from "../cipher";

$( () => {
    let $txtKey = $( "#txtKey" );
    let $txtCipher = $( "#txtCipher" );
    let $txtResults = $( "#txtResults" );
    let key = $txtKey.val().toString().trim().replace( /[^a-z]/gi, "" );
    let cipherText = $txtCipher.val().toString().trim().replace( /[^a-z]/gi, "" );
    let results = "Method 1:\n";

    results += decodeColumnar( cipherText, key ) + "\n";
    results += "\n\nMethod 2:\n";
    results += decodeColumnar2( cipherText, key ) + "\n";
    results += "\n\nMethod 3:\n";
    results += decodeColumnar3( cipherText, key ) + "\n";
    results += "\n\nMethod 4:\n";
    results += decodeColumnar4( cipherText, key ) + "\n";

    $txtResults.val( results );
} );