declare interface String {
    replaceAt( index: number, replacement: string ): string;
}

String.prototype.replaceAt = function ( this: string, index: number, replacement: string ) {
    if ( index >= this.length ) {
        throw new RangeError( "Cannot replace character outside bounds of string" );
    }

    if ( replacement.length !== 1 ) {
        throw new Error( "Replacement string must be a single character" );
    }

    return Array.from( this ).map( ( c, i ) => i === index ? replacement : c ).join( "" );
};

declare interface Array<T> {
    randomize(): T[];
}

Array.prototype.randomize = function <T>( this: T[] ) {
    let newArray: T[] = [];
    let indices = [ ...new Array( this.length ) ].map( ( _, i ) => i );

    for ( let i = 0; i < this.length; i++ ) {
        let [ randomIndex ] = indices.splice( Math.floor( Math.random() * indices.length ), 1 );

        newArray.push( this[ randomIndex ] );
    }

    return newArray;
}