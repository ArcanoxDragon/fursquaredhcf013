export function findOrder( input: string ) {
    input = input.replace( /[^a-z]/ig, "" ).toLowerCase();

    let alphabet = new Array( 26 )
        .fill( "a".charCodeAt( 0 ) )
        .map( ( v, i ) => String.fromCharCode( v + i ) );
    let tokens: string[] = [];

    for ( let char of Array.from( input ) ) {
        let existing = tokens.filter( c => c.charAt( 0 ) === char );
        let token = char;

        if ( existing.length > 0 ) {
            let second = alphabet[ existing.length ];

            token += second;
        }

        tokens.push( token );
    }

    let sorted = Array.from( tokens ).sort();

    //return tokens.map( t => sorted.indexOf( t ) );
    return sorted.map( t => tokens.indexOf( t ) );
}

if ( require.main === module ) {
    if ( process.argv.length < 3 ) {
        throw new Error( "Missing input" );
    }

    let [ , , input ] = process.argv;
    let positions = findOrder( input );

    console.log( positions.join( "," ) );
}